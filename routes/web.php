<?php

use Illuminate\Support\Facades\Route;
use App\User;
use App\Permissions\Role;
use App\Permissions\Permission;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

// Ruta inicion
// Ruta index
Route::get('/home', 'HomeController@index')->name('home');

// Ruta gestion de roles
// Ruta gestion de roles
Route::resource('/role', 'RoleController')->names('role');

// Ruta gestion de usuarios
// User management route
Route::resource('/user', 'UserController',['except'=>[
    'create','store']])->names('user');