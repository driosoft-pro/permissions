<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>
    <h3>**Roles And Permisos**</h3>
    <p>
        Bienvenidos al proyecto "Permisos". El proyecto es un sistema completo de roles y permisos en Laravel 7.
    </p>
    <h4>**Requisitos:**</h4>
    <p>
        1. Versión de PHP mayor o igual a la 7.2.5
    </p>
    Documentación oficial de Laravel: https://laravel.com/docs/7.x
    <p>
        2. Tener instalado composer en tu equipo: https://getcomposer.org/
    </p>
        <p>• En Windows puedes descargar el programa composer desde la página oficial:
            https://getcomposer.org/Composer-Setup.exe</p>
        <p>• Linux</p>
            <p>- Debian: sudo get install composer.</p>
            <p>- Ubuntu: sudo apt-get install composer.</p>
            <p>- Arch Linux: sudo pacman -S composer.</p>
            <p>- Fedora: sudo yum install composer</p>
        <p>• En Mac puedes instalar composer con el siguiente comando</p>
            <p>- Mac: brew install composer</p>
    <p>
        3. Tener instalado git: https://gitforwindows.org/
    </p>
        <p>• En Windows puedes descargar el programa git desde la página oficial: https://git-scm.com/</p>
        <p>• Linux:</p>
            <p>- Debian: sudo get install git.</p>
            <p>- Ubuntu: sudo apt-get install git. </p>
            <p>- Arch Linux: sudo pacman -S git. </p>
            <p>- Fedora: sudo yum install git.</p>
        <p>• En Mac puedes descargar el programa git desde la página oficial: https://git-scm.com/download/mac</p>
    <p>
        4. Tener instalado un servidor local:
    </p>
        <p>• XAMPP: https://www.apachefriends.org/es/index.html (Todas plataformas).</p>
        <p>• WampServer: https://www.wampserver.com/en/ (Solo Windows).</p>
        <p>• Laragon: https://laragon.org/download/index.html (Solo windows).</p>
    <h5>
        Listo ya podrás instalar este proyecto.
    </h5>
    <h4>**Instalación de proyecto correctamente:**</h4>
    <p>
        1. Descarga o clónalo el proyecto:
    </p>
        <p>• git clone git://gitlab.com/driosoft-pro/permissions</p>
    <p>
        2. Ejecuta el comando:
    </p>
        <p>• composer install</p>
    <p>
        3. para copiar el archivo .env.example y pegarlo con el nombre. env. ejecuta siguiente comando:
    </p>
        <p>• cp .env.example .env</p>
        <p>• copy .env.example .env (Solo windows)</p>
    <p>
        4. Debes crear una nueva llave para proyecto ejecuta el siguiente comando:
    </p>
        <p>• php artisan key:generate</p>
    <p>
        5. Configura la base de datos modificando el archivo .env
    </p>
        <p>DB_CONNECTION=mysql</p>
        <p>DB_HOST=127.0.0.1</p>
        <p>DB_PORT=3306</p>
        <p>DB_DATABASE=permissions</p>
        <p>DB_USERNAME=root</p>
        <p>DB_PASSWORD=</p>
    <p>
        6. Para efectuar los cambios previamente realizados ejecuta el siguiente comando
    </p>
        <p>• php artisan migrate</p>
    <p>
        7. Añadir dependencias de npm con el siguiente comando
    </p>
        <p>• npm install && npm run dev</p>
    <p>
        8. Navega desde una consola hasta la carpeta del proyecto (No importa el sistema operativo) y ejecuta el
        siguiente comando.
    </p>
        <p>• php artisan serve </p>
    <p>
        9. ¡¡Listo eso es todo!!
    </p>
    <h6>
        DIOS te bendiga, espero que este pequeño proyecto te sirve y sea de tu agrado.
    </h6>
