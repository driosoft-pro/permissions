<?php

use App\User;
use App\Permissions\Role;
use App\Permissions\Permission;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Db;
use Illuminate\Support\Facades\Hash;

class PermissionInfoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
public function run()
    {
        // Truncate tables mode DB
        // desactivar llevas de bd
        // disable foreing keys
        DB::statement("SET foreign_key_checks=0");
            DB::table('role_user')->truncate();
            DB::table('permission_role')->truncate();
            Role::truncate();
            Permission::truncate();
        // activar llevas de bd
        // enable foreing keys
        DB::statement("SET foreign_key_checks=1");

        // Buscar usuario administrador
        // Search Administrator user
        $userAdmin= User::where('email','admin@admin.com')->first();

        // Verificacion de usuario administrador
        // User verification Administrator
        if ($userAdmin) {
            $userAdmin->delete();
        }

        // Creacion de usuario administrador  
        // Creation of user Administrator  
        $userAdmin= User::create([
            'name'      => 'Admin',
            'email'     => 'admin@admin.com',
            'password'  => Hash::make('Secret')    
        ]);

        // Rol administrador
        // Administrator role
        $rolAdmin= Role::create([
            'name' => 'Admin',
            'slug' => 'admin',
            'description' => 'Administrator',
            'full-access' => 'yes'
        ]);
        
        // Asignacion de rol a usuario   
        // Assignment of role to user
        $userAdmin->roles()->sync([ $rolAdmin->id ]);

        //permisos de roles y usuarios
        // user role permissions
        $permission_all = [];
        
        // Permisos de roles
        // Role Permissions
        $permission = Permission::create([
            'name' => 'List role',
            'slug' => 'role.index',
            'description' => 'A user can list role',
        ]);
        
        // asignar permiso al rol
        // assign permission to the role
        $permission_all[] = $permission->id;
                
        $permission = Permission::create([
            'name' => 'Show role',
            'slug' => 'role.show',
            'description' => 'A user can see role',
        ]);

        // asignar permiso al rol
        // assign permission to the role
        $permission_all[] = $permission->id;
                
        $permission = Permission::create([
            'name' => 'Create role',
            'slug' => 'role.create',
            'description' => 'A user can create role',
        ]);

        // asignar permiso al rol
        // assign permission to the role        
        $permission_all[] = $permission->id;
                
        $permission = Permission::create([
            'name' => 'Edit role',
            'slug' => 'role.edit',
            'description' => 'A user can edit role',
        ]);

        // asignar permiso al rol
        // assign permission to the role
        $permission_all[] = $permission->id;
                
        $permission = Permission::create([
            'name' => 'Destroy role',
            'slug' => 'role.destroy',
            'description' => 'A user can destroy role',
        ]);


        // Permisos de Usuarios
        // User Permissions

        // asignar permiso al usuario
        // assign permission to the user        
        $permission_all[] = $permission->id;
            
        //permission user
        $permission = Permission::create([
            'name' => 'List user',
            'slug' => 'user.index',
            'description' => 'A user can list user',
        ]);
        
        // asignar permiso al usuario
        // assign permission to the user         
        $permission_all[] = $permission->id;
        
        $permission = Permission::create([
            'name' => 'Show user',
            'slug' => 'user.show',
            'description' => 'A user can see user',
        ]);        
        
        // asignar permiso al usuario
        // assign permission to the user         
        $permission_all[] = $permission->id;
        
        $permission = Permission::create([
            'name' => 'Edit user',
            'slug' => 'user.edit',
            'description' => 'A user can edit user',
        ]);
        
        // asignar permiso al usuario
        // assign permission to the user         
        $permission_all[] = $permission->id;
        
        $permission = Permission::create([
            'name' => 'Destroy user',
            'slug' => 'user.destroy',
            'description' => 'A user can destroy user',
        ]);
   
        

        // asignar permiso al usuario
        // assign permission to the user         
        $permission_all[] = $permission->id;

        //new
        $permission = Permission::create([
            'name' => 'Show own user',
            'slug' => 'userown.show',
            'description' => 'A user can see own user',
        ]);        
        
        $permission_all[] = $permission->id;
        
        $permission = Permission::create([
            'name' => 'Edit own user',
            'slug' => 'userown.edit',
            'description' => 'A user can edit own user',
        ]);
        
    }
}

