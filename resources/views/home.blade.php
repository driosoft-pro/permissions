@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    Welcome ! <br> 
                    You are using the role and permission manager for Laravel 7 with PHP 7.3 and MySQL
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
