<?php

namespace App\Traits;

trait UserTrait {

    // union de tabl
    // table join
    public function roles()
    {
        return $this->belongsToMany('App\Permissions\Role')->withTimesTamps();
    }

    // permission validation
    // validacion de permisos
    public function havePermission($permission){
        
        foreach ($this->roles as $role ) {
            
            if ($role['full-access'] =='yes' ) {
                return true;
            }

            foreach ($role->permissions as $perm) {
                
                if ($perm->slug == $permission ) {
                    return true;
                }   
            }
        }
        return false;
    }
}

