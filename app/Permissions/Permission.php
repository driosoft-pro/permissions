<?php

namespace App\Permissions;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'slug', 'description', 
    ];
    
    // New code
    // union de tablas
    // table join
    public function roles(){
        return $this->belongsToMany('App\Permissions\Role')->withTimesTamps();
    }

}

