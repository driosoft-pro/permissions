<?php

namespace App\Permissions;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'slug', 'description', 'full-access',
    ];

    // New code
    // union de tablas
    // table join
    public function users(){
        return $this->belongsToMany('App\User')->withTimesTamps();
    }
    
    // union de tablas
    // table join
    public function permissions(){
        return $this->belongsToMany('App\Permissions\Permission')->withTimesTamps();
    }
}
